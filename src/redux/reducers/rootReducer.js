// import external modules
import { combineReducers } from "redux";
import loginReducer from './login/loginReducer';
import LoaderReducer from './loader/loaderReducer';

const rootReducer = combineReducers({ 
   login:loginReducer,
   loader:LoaderReducer,
   
});

export default rootReducer;
